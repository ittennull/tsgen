﻿using System;

namespace TypescriptDeclarations.Attributes
{
    [AttributeUsage(AttributeTargets.Enum)]
    public class TsEnumAttribute : Attribute
    {
        public string Name { get; set; }
        public string Module { get; set; }
    }
}
