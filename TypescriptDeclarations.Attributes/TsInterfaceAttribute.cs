﻿using System;

namespace TypescriptDeclarations.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class TsInterfaceAttribute : Attribute
    {
        public string Name { get; set; }
        public string Module { get; set; }
    }
}
