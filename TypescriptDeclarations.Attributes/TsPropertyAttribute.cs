﻿using System;

namespace TypescriptDeclarations.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class TsPropertyAttribute : Attribute
    {
        public string Name { get; set; }
        public bool Optional { get; set; }
        public bool Ignore { get; set; }
    }
}
