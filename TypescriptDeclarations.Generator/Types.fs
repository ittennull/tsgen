﻿namespace Types
open System
open TypescriptDeclarations.Attributes


type TsAttributeType = 
    | InterfaceAttr of TsInterfaceAttribute
    | EnumAttr of TsEnumAttribute

type ExportType = {
    clrType: Type
    tsAttribute: TsAttributeType option
}

type TsType = 
    | Number
    | Boolean
    | String
    | Date
    | Optional of Type
    | Array of Type
    | Custom of Type

