﻿module TypeExport
open System
open System.Reflection
open Types
open Scan
open TypescriptDeclarations.Attributes


type private TypeClassification = Interface | Enum

let private indent = "    "

let private getName t =
    let customName = 
        match t.tsAttribute with
        | Some (InterfaceAttr attr) -> attr.Name
        | Some (EnumAttr attr) -> attr.Name
        | _ -> ""

    match customName with
    | x when not (String.IsNullOrEmpty(x)) -> x
    | _ -> t.clrType.Name


let private getModuleName defaultModuleName t =
    let nameFromAttribute = 
        match t.tsAttribute with
        | Some (InterfaceAttr attr) -> Option.ofObj attr.Module
        | Some (EnumAttr attr) -> Option.ofObj attr.Module
        | _ -> None

    nameFromAttribute 
        |> Option.orElse defaultModuleName
        |> Option.defaultValue t.clrType.Namespace


let private toCamelCase (str: string) = Char.ToLowerInvariant(str.[0]).ToString() + str.[1..]


let rec private getTypeName exportTypeToModuleName clrType = 
    match getTsType clrType with
    | Number -> "number"
    | Boolean -> "boolean"
    | String -> "string"
    | Date -> "Date"
    | Optional t -> getTypeName exportTypeToModuleName t
    | Array t -> (getTypeName exportTypeToModuleName t) + "[]"
    | Custom t ->
        let (exportType, moduleName) = exportTypeToModuleName |> List.find (fun (exportType, _) -> exportType.clrType = t)
        moduleName + "." + (getName exportType)


let private getPropertyName (propertyAttr: TsPropertyAttribute option) (propertyInfo: PropertyInfo) =
    let name = match propertyAttr with
                | Some attr when not (String.IsNullOrEmpty(attr.Name)) -> attr.Name
                | _ -> toCamelCase propertyInfo.Name
    
    let isOptionalByTsAttribute =
        match propertyAttr with
        | Some attr when attr.Optional -> true
        | _ -> false

    let isOptionalByPropertyType =
        match getTsType propertyInfo.PropertyType with
        | Optional _ -> true
        | _ -> false

    match isOptionalByTsAttribute || isOptionalByPropertyType with
    | true  -> name + "?"
    | false -> name


let private exportAsInterface exportTypeToModuleName t =
    let start = "interface " + (getName t) + " {"
    let propertyLines = 
        t.clrType.GetProperties(BindingFlags.Instance ||| BindingFlags.Public)
        |> List.ofArray
        |> List.map (fun x -> x, x.GetCustomAttribute<TsPropertyAttribute>() |> Option.ofObj)
        |> List.filter (function
                        | (_, Some attr) when attr.Ignore -> false
                        | _ -> true )
        |> List.map (fun (propInfo, propAttr) -> 
                            let propertyName = getPropertyName propAttr propInfo
                            let propertyType = getTypeName exportTypeToModuleName propInfo.PropertyType
                            propertyName, propertyType )
        |> List.map (fun (name, typeString) -> indent + name + ": " + typeString + ";")
    let ``end`` = "}"
    [start] @ propertyLines @ [``end``]


let private exportAsEnum t =
    let start = "export enum " + (getName t) + " {"

    let enumNames = Enum.GetNames(t.clrType) |> List.ofArray
    let enumValue = 
        let values = Enum.GetValues(t.clrType) 
        List.init values.Length values.GetValue
        |> List.map (fun x -> System.Convert.ChangeType(x, Enum.GetUnderlyingType(t.clrType)).ToString())

    let nameValuePairs = List.zip enumNames enumValue
    let enumLines = 
        nameValuePairs
        |> List.mapi (fun i (name, value) -> 
                        let comma = match (i = nameValuePairs.Length - 1) with false -> "," | _ -> ""
                        indent + name + " = " + value + comma )

    let ``end`` = "}"
    [start] @ enumLines @ [``end``]


let private exportType exportTypeToModuleName t =
    let typeClassification =
        match t.tsAttribute with
        | Some (InterfaceAttr _) -> Interface
        | Some (EnumAttr _) -> Enum
        | _ -> match t.clrType.IsEnum with true -> Enum | false -> Interface

    match typeClassification with
    | Interface -> exportAsInterface exportTypeToModuleName t
    | Enum -> exportAsEnum t


let private exportModule exportTypeToModuleName moduleName types =
    let moduleContent = 
        types
        |> List.collect (exportType exportTypeToModuleName)
        |> List.map ((+) indent)
        |> String.concat "\n"

    "declare module " + moduleName + " {\n" + moduleContent + "\n}"


let exportTypes defaultModuleName exportTypes =
    let exportTypeToModuleName = exportTypes |> List.map (fun t -> t, getModuleName defaultModuleName t)
    let exportTypesPerModule = exportTypes |> List.groupBy (fun t -> getModuleName defaultModuleName t)

    exportTypesPerModule
    |> List.map (fun (moduleName, exportTypes) -> exportModule exportTypeToModuleName moduleName exportTypes)
    |> String.concat "\n\n"
