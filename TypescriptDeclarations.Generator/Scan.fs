﻿module Scan
open System
open System.Reflection
open System.Linq
open TypescriptDeclarations.Attributes
open Types
open System.Collections.Generic


let private getExportType (t: Type) =
    t.GetCustomAttribute<TsInterfaceAttribute>() 
    |> Option.ofObj
    |> Option.map (fun attr -> {clrType = t; tsAttribute = Some (InterfaceAttr attr)})
    |> Option.orElseWith (fun () -> 
        t.GetCustomAttribute<TsEnumAttribute>() 
        |> Option.ofObj
        |> Option.map (fun attr -> {clrType = t; tsAttribute = Some (EnumAttr attr)}))
    |> Option.defaultValue {clrType = t; tsAttribute = None}


let private enumerablePrefix = "System.Collections.Generic.IEnumerable`1"

let getTsType =
    function
    | t when    t = typeof<float> || 
                t = typeof<float32> ||
                t = typeof<double> || 
                t = typeof<decimal> || 
                t = typeof<int8> || 
                t = typeof<int16> || 
                t = typeof<int32> || 
                t = typeof<int64> || 
                t = typeof<uint8> ||
                t = typeof<uint16> ||
                t = typeof<uint32> ||
                t = typeof<uint64> ||
                t = typeof<byte> -> Number
    | t when    t = typeof<string> -> String
    | t when    t = typeof<bool> -> Boolean
    | t when    t = typeof<DateTime> -> Date
    | t when    t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<Nullable<_>> -> Optional (t.GenericTypeArguments.First())
    | t when    t.IsGenericType && t.GetGenericTypeDefinition() = typedefof<IEnumerable<_>> -> Array (t.GenericTypeArguments.First())
    | t when t.GetInterfaces()
                .Where(fun x -> x.IsGenericType)
                .Any(fun x -> x.GetGenericTypeDefinition().FullName.StartsWith(enumerablePrefix)) -> 
                    let generics = 
                        t.GetInterfaces()
                            .Where(fun x -> x.IsGenericType)
                            .First(fun x -> x.GetGenericTypeDefinition().FullName.StartsWith(enumerablePrefix));
                    Array (generics.GenericTypeArguments.Single())
    | t -> Custom t

let private isNewType existingExportTypes clrType =
    existingExportTypes
    |> List.exists (fun x -> x.clrType = clrType)  
    |> not

let rec private getTypesForExport isTopLevelType existingExportTypes clrType =
    match getTsType clrType with
    | Optional toExport -> (getTypesForExport isTopLevelType) existingExportTypes toExport
    | Array toExport -> (getTypesForExport isTopLevelType) existingExportTypes toExport
    | Custom toExport when isNewType existingExportTypes toExport -> 
        let currentExportType = getExportType toExport

        if isTopLevelType && currentExportType.tsAttribute = None then
            existingExportTypes
        else
            let propertyTypes = 
                toExport.GetProperties(BindingFlags.Instance ||| BindingFlags.Public)
                |> List.ofArray
                |> List.map (fun x -> x.PropertyType) 
                |> List.distinct
        
            let newExistingExportTypes = currentExportType :: existingExportTypes
            List.fold (getTypesForExport false) newExistingExportTypes propertyTypes
    | _ -> existingExportTypes


let getAllTypesForExport allTypes =
    List.fold (getTypesForExport true) [] allTypes
