﻿using TypescriptDeclarations.Attributes;

namespace MyLibrary
{
    [TsInterface]
    public class SomeClass
    {
        public string Name { get; set; }
        public int? Number { get; set; }
        public int[] Things { get; set; }
        public SomeEnum SomeThing { get; set; }
    }

    [TsEnum]
    public enum SomeEnum
    {
        A,
        B = 2,
        C
    }
}
