﻿using TypescriptDeclarations.Attributes;

namespace TestLibrary
{
    [TsInterface(Module = "M1")]
    public interface ISomething
    {
        int Number { get; set; }

        void foo(int asd);
    }

    public class NoAttributes
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public int[] IntArray { get; set; }
    }


    [TsInterface]
    public class Class1
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public int[] IntArray { get; set; }
    }

    [TsInterface(Name = "modifiedClass2", Module = "M2")]
    public class Class2
    {
        [TsProperty(Ignore = true)]
        public string Name { get; set; }
        [TsProperty(Optional = true)]
        public int Number { get; set; }
        public int[] IntArray { get; set; }

        [TsProperty(Name = "DCO")]
        public DependencyClass2 DependencyClass2Object { get; set; }
        public DependencyClass2 DependencyClass2Object2 { get; set; }
    }

    public class DependencyClass2
    {
        public int SomeInt { get; set; }

        public DependencyClass3[] ArrayOfDependencyClass3s { get; set; }
    }

    [TsInterface(Module = "M3", Name = "d3")]
    public class DependencyClass3
    {
        public int SomeInt { get; set; }

        public Enum2 Enum2 { get; set; }
    }

    [TsEnum(Module = "M3")]
    enum Enum1
    {
        One,
        Two
    }


    public enum Enum2
    {
        Monday = 56,
        Tuesday = 99
    }

    public enum Enum3
    {
        Asd
    }
}
