# tsdeclgen
[.NET Core Global Tool](https://docs.microsoft.com/en-us/dotnet/core/tools/global-tools) that converts C# classes and enums marked with special attributes to TypeScript declarations that can be later imported to TypeScript project.
For example:
```csharp
namespace MyLibrary
{
    [TsInterface]
    public class SomeClass
    {
        public string Name { get; set; }
        public int? Number { get; set; }
        public int[] Things { get; set; }
        public SomeEnum SomeThing { get; set; }
    }

    [TsEnum]
    public enum SomeEnum
    {
        A,
        B = 2,
        C
    }
}
```
becomes:
```typescript
declare module MyLibrary {
    export enum SomeEnum {
        A = 0,
        B = 2,
        C = 3
    }
    interface SomeClass {
        name: string;
        number?: number;
        things: number[];
        someThing: MyLibrary.SomeEnum;
    }
}
```

## How to use
1. Add nuget package `TypescriptDeclarations.Attributes` to your project that has types you want to use in TypeScript (usually it's DTOs). Mark these types with attributes from the package (see example above)

2. Install the tool `TypescriptDeclarations.Generator` using [dotnet tool install](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-tool-install):
   ```powershell
   dotnet tool install -g TypescriptDeclarations.Generator
   ```

3. Use the tool:
   ```powershell
   tsdeclgen -in "project\bin\WebApp.Models.dll" -out "WebAppModels.d.ts" -module "Dtos"
   ```
    Note: you might need to publish you project first and point tsdeclgen to dll in published folder:
    ```powershell
    dotnet publish
    tsdeclgen -in "project\bin\publish\WebApp.Models.dll" -out "WebAppModels.d.ts"
    ```

## Command line
tsdeclgen [options]


Options:
- _-module_ &nbsp;&nbsp;&nbsp;&nbsp; default namespace for output declarations
- _-out_ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; file path to write output to. If not specified it will print the output to console
- _-in_ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; path to .dll file witch contains exported types (the option can be specified multiple times)
- _--help_ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; show help